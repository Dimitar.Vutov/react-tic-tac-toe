export default function LeaderBoard(props) {
    const initializeLeaderBoard = () => {
        const winner = props.xWins > props.oWins ? 'x' : 'o';
        if(winner === 'x') {
            return (
                <div>
                <p>X won {props.xWins} times</p>
                <p>O won {props.oWins} times</p>
                </div>
            )
        } else {
            return (
                <div>
                    <p>O won {props.oWins} times</p>
                    <p>X won {props.xWins} times</p>
                </div>
            )
        }
    }
    return (
        <div>
            {initializeLeaderBoard()}
        </div>
    )
}