import {useState, useEffect} from 'react';
import { useParams } from 'react-router';
import { useHistory } from "react-router-dom";
export default function Tabs (props) {
    const history = useHistory();
    const params = useParams();
    const [state, setState] = useState({
        name: props.name,
        isToggled: false
    })
    const toggle = () => {
        console.log(props.path)
        history.push('/' + props.path);
    }
    useEffect(() => {
        console.log(params.active_tab)
        if(params.active_tab === props.path) {
            setState({
                name: props.name,
                isToggled: true
            })
        } else {
            setState({
                name: props.name,
                isToggled: false
            })
        }
    }, [params, props])
    return (
        <div className="tab" >
            <h1 onClick={toggle}>
            {state.name}</h1>
            <div hidden={!state.isToggled}>
            {props.children}
            </div>
        </div>
    )
}