import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Tabs from './components/tabs';
import LeaderBoard from './components/leaderBoard';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
function Square(props) {
    return (
      <button className="square" onClick={props.onClick}>
        {props.value}
      </button>
    );
}

function Board(props) {
  const renderSquare = (i) => {
    return <Square value={props.squares[i]} onClick={() => props.onClick(i)}/>;
  }

       return (
        <div>
        <div className="board-row">
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className="board-row">
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className="board-row">
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
      </div>
    );
}

function Game(props) {
  const [leaderBoardState, setLeaderBoardState] = useState({
    xWins: 0,
    oWins: 0,
    winner: ''
  })
    const [historyState, setHistory] = useState({
        history: [{
          squares: Array(9).fill(null)
        }],
      })
      const [stepState, setStep] = useState({
        stepNumber: 0,
        xIsNext: true
      })
  const handleClick = (i) => {
    const history = historyState.history.slice(0, stepState.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    
    if(calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = stepState.xIsNext === true ? 'X' : 'O';
    setHistory({
      history: history.concat([{
        squares: squares,
      }]),
    });
    setStep({
      stepNumber: history.length,
      xIsNext: !stepState.xIsNext,
    })
  }
  const jumpTo = (step) => {
    if(step === 0) {
      setLeaderBoardState({
        oWins: leaderBoardState.oWins,
        xWins: leaderBoardState.xWins,
        winner: ''
      })
    }
    setStep({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }
    const history = historyState.history;
    const current = history[stepState.stepNumber];
    const winner = calculateWinner(current.squares);
    const moves = history.map((step, move) => {
      const desc = move ?
      'Go to move # ' + move :
      'Go to game start';
      return (
      <li key={move}>
          <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
      );
    });
    let status;
    if (winner) {
      status = 'Winner: ' + winner;
      if(winner === 'X' && !leaderBoardState.winner) {
        setLeaderBoardState({
          xWins: leaderBoardState.xWins + 1,
          oWins: leaderBoardState.oWins,
          winner: winner
        })
      } else if(!leaderBoardState.winner) {
        setLeaderBoardState({
          xWins: leaderBoardState.xWins ,
          oWins: leaderBoardState.oWins + 1,
          winner: winner
        })
      }
    } else {
      status = 'Next player: ' + (stepState.xIsNext ? 'X' : 'O');
    }
    return (
      <Router>
      <div className='tabs'>
        <Switch>
        <Route path='/:active_tab?'>
      <Tabs path='game' name="Game">
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => {handleClick(i)}}
            />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
      </Tabs>
        <Tabs path='leaderboard' name="Leaderboard">
        <LeaderBoard xWins={leaderBoardState.xWins} oWins={leaderBoardState.oWins}></LeaderBoard>
      </Tabs>
      </Route>
      </Switch>
      </div>
      
      </Router>
    );
  
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}